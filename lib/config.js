/*
The system makes heavy use of dependency injection.  This file wires up all the
dependencies, making use of the environment.
*/

const MessageStore = require('@suchsoftware/proof-of-concept-message-store')

const PostgresClient = require('./postgres-client')
const RegistrationComponent = require('./registration-component')

function createConfig ({ env }) {
  const postgresClient = PostgresClient({
    connectionString: env.messageStoreConnectionString
  })
  const messageStore = MessageStore({ session: postgresClient })

  const registrationComponent = RegistrationComponent({ messageStore })

  const components = [
    registrationComponent
  ]

  return {
    env,
    messageStore,
    components,
    registrationComponent
  }
}

module.exports = createConfig
