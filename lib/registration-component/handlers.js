const uuid = require('uuid/v4')

const projection = require('./projection')
const Registration = require('./registration')

function build ({ messageStore }) {
  return {
    async Register (register) {
      const userId = register.data.userId
      const registration = new Registration()

      await messageStore.fetch(
        `registration-${userId}`,
        projection,
        registration
      )

      if (registration.registered()) {
        return
      }

      // We'll skip validation of the inputs because that's not the point of
      // this demo.
      const registered = {
        id: uuid(),
        type: 'Registered',
        metadata: {
          traceId: register.metadata.traceId
        },
        data: {
          ...register.data
        }
      }

      return messageStore.write(
        `registration-${userId}`,
        registered,
        registration.version
      )
    },

    async Name (name) {
      const userId = name.data.userId
      const registration = new Registration()

      await messageStore.fetch(
        `registration-${userId}`,
        projection,
        registration
      )

      if (registration.processed(name.globalPosition)) {
        return
      }

      // We'll skip validation of the inputs because that's not the point of
      // this demo.
      const named = {
        id: uuid(),
        type: 'Named',
        metadata: {
          traceId: name.metadata.traceId
        },
        data: {
          ...name.data
        }
      }

      return messageStore.write(
        `registration-${userId}`,
        named,
        registration.version
      )
    },

    async Gdpr (gdpr) {
      const userId = gdpr.data.userId
      const registration = new Registration()

      await messageStore.fetch(
        `registration-${userId}`,
        projection,
        registration
      )

      if (registration.forgotten()) {
        return
      }

      // We'll skip validation of the inputs because that's not the point of
      // this demo.
      const gdprd = {
        id: uuid(),
        type: 'Gdprd',
        metadata: {
          traceId: gdpr.metadata.traceId
        },
        data: {
          ...gdpr.data
        }
      }

      return messageStore.write(
        `registration-${userId}`,
        gdprd,
        registration.version
      )
    }
  }
}

module.exports = build
