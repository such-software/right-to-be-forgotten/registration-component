class Registration {
  id
  email
  registeredDate
  sequence
  forgottenTime

  registered () {
    return !!this.registeredDate
  }

  processed (globalPosition) {
    return this.sequence >= globalPosition
  }

  forgotten () {
    return !!this.forgottenTime
  }
}

module.exports = Registration
