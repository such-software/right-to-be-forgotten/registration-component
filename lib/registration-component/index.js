const Handlers = require('./handlers')

function build({ messageStore }) {
  const handlers = Handlers({ messageStore })

  const subscription = messageStore.createSubscription({
    streamName: 'registration:command',
    handlers,
    subscriberId: 'registration:command'
  })

  function start() {
    subscription.start()
  }

  return {
    handlers,
    start
  }
}

module.exports = build
