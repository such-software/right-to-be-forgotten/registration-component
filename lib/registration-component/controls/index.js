const commands = require('./commands')
const events = require('./events')
const email = require('./email')
const id = require('./id')
const password = require('./password')
const time = require('./time')

module.exports = {
  commands,
  events,
  email,
  id,
  password,
  time
}
