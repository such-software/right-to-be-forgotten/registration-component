module.exports = {
  example () {
    return this.raw.toISOString()
  },

  raw () {
    return new Date()
  },

  Date: {
    example () {
      return '2000-01-01'
    }
  },

  Effective: {
    example () {
      return '2000-01-01T12:00:01'
    }
  }
}
