const timeControls = require('../time')
const idControls = require('../id')

module.exports = {
  example () {
    return {
      id: idControls.example(),
      type: 'Gdpr',
      metadata: {
        traceId: idControls.example(),
      },
      data: {
        userId: idControls.example(),
        effectiveTime: timeControls.Effective.example()
      }
    }
  }
}
