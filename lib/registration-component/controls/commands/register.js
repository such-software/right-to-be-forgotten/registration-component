const emailControls = require('../email')
const timeControls = require('../time')
const idControls = require('../id')
const passwordControls = require('../password')

module.exports = {
  example () {
    return {
      id: idControls.example(),
      type: 'Register',
      metadata: {
        traceId: idControls.example(),
      },
      data: {
        userId: idControls.example(),
        email: emailControls.example(),
        registeredDate: timeControls.Date.example(),
        passwordHash: passwordControls.example()
      }
    }
  }
}
