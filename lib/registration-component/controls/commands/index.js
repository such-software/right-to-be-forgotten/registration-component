const gdpr = require('./gdpr')
const name = require('./name')
const register = require('./register')

module.exports = {
  gdpr,
  name,
  register
}
