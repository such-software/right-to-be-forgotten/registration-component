const idControls = require('../id')

module.exports = {
  example () {
    return {
      id: idControls.example(),
      type: 'Name',
      metadata: {
        traceId: idControls.example()
      },
      data: {
        userId: idControls.example(),
        name: 'Ethan Garofolo'
      }
    }
  }
}
