const gdprd = require('./gdprd')
const named = require('./named')
const registered = require('./registered')

module.exports = {
  gdprd,
  named,
  registered
}
