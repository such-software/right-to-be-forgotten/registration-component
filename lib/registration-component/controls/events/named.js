const idControls = require('../id')

module.exports = {
  example () {
    return {
      id: idControls.example(),
      type: 'Named',
      metadata: {
        traceId: idControls.example(),
      },
      data: {
        userId: idControls.example(),
        name: 'Ethan Garofolo'
      }
    }
  }
}
