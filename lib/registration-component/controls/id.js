const { v4: uuid } = require('uuid')

module.exports = {
  example () {
    return uuid()
  }
}
