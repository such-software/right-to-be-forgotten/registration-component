module.exports = {
  Registered (registration, registered) {
    registration.id = registered.data.userId,
    registration.email = registered.data.email,
    registration.registeredDate = new Date(registered.data.registeredDate)
    registration.sequence = registered.globalPosition
  },

  Named (registration, named) {
    registration.name = named.data.name
    registration.sequence = named.globalPosition
  },

  Gdprd (registration, gdprd) {
    registration.forgottenTime = gdprd.data.effectiveTime
    registration.sequence = gdprd.globalPosition
  }
}
