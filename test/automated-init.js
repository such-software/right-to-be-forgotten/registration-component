// eslint-disable-next-line import/no-extraneous-dependencies
const Bluebird = require('bluebird')
const test = require('blue-tape')
// eslint-disable-next-line import/no-extraneous-dependencies

const { config } = require('../lib')

test.onFinish(() => {
  config.messageStore.stop()
})

/* eslint-disable no-console */
process.on('unhandledRejection', err => {
  console.error('Uh-oh. Unhandled Rejection')
  console.error(err)

  process.exit(1)
})
/* eslint-enable no-console */

function createMessageStoreWithWriteSink (sink) {
  const writeSink = (stream, message, expectedVersion) => {
    sink.push({ stream, message, expectedVersion })

    return Promise.resolve(true)
  }

  return { ...config.messageStore, write: writeSink }
}

module.exports = {
  config,
  createMessageStoreWithWriteSink
}
