const test = require('blue-tape')
const uuid = require('uuid/v4')

const { config } = require('../../../automated-init')

test('It writes a Named event', t => {
  const userId = uuid()
  const newName = 'Ethan Garofolo'

  const name = {
    id: uuid(),
    type: 'Name',
    metadata: {
      traceId: uuid()
    },
    data: {
      userId,
      name: newName,
    },
    globalPosition: 1
  }

  return config.registrationComponent.handlers.Name(name)
    .then(() => config.registrationComponent.handlers.Name(name))
    .then(() =>
      config.messageStore.read(`registration-${userId}`)
        .then(messages => {
          t.equal(messages.length, 1, 'It wrote 1 message')

          const [named] = messages

          t.equal(named.type, 'Named', 'Named event')
          t.equal(named.data.userId, userId, 'Correct userId')
          t.equal(named.data.name, newName, 'Correct name')
        })
    )
})
