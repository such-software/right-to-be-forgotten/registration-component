const test = require('blue-tape')
const uuid = require('uuid/v4')

const registrationControls = require('../../../../lib/registration-component/controls')

const { config } = require('../../../automated-init')

test('It writes a Gdprd event', t => {
  const gdpr = registrationControls.commands.gdpr.example()
  gdpr.globalPosition = 1
  const { userId, effectiveTime } = gdpr.data

  return config.registrationComponent.handlers
    .Gdpr(gdpr)
    .then(() => config.registrationComponent.handlers.Gdpr(gdpr))
    .then(() =>
      config.messageStore.read(`registration-${userId}`).then(messages => {
        t.equal(messages.length, 1, 'It wrote 1 message')

        const [gdprd] = messages

        t.equal(gdprd.type, 'Gdprd', 'Gdprd event')
        t.equal(gdprd.data.userId, userId, 'Correct userId')
        t.equal(gdprd.data.effectiveTime, effectiveTime, 'Correct time')
      })
    )
})
