const test = require('blue-tape')

const registrationControls = require('../../../../lib/registration-component/controls')

const { config } = require('../../../automated-init')

test('It writes a Registered event', t => {
  const register = registrationControls.commands.register.example()
  const userId = register.data.userId

  return config.registrationComponent.handlers.Register(register)
    .then(() => config.registrationComponent.handlers.Register(register))
    .then(() =>
      config.messageStore.read(`registration-${userId}`)
        .then(messages => {
          t.equal(messages.length, 1, 'It wrote 1 message')

          const [registered] = messages

          t.equal(registered.type, 'Registered', 'Registered event')
          t.equal(registered.data.userId, register.data.userId, 'Correct userId')
        })
    )
})
